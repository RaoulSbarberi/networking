import sys
import socket

network_protocol_option = ""  # IPv4 or IPv6
while network_protocol_option != "IPv4" and network_protocol_option != "IPv6":
    network_protocol_option = input("Select Network Protocol (IPv4/IPv6): ")

transport_protocol_option = ""  # TCP or UDP
while transport_protocol_option != "TCP" and transport_protocol_option != "UDP":
    transport_protocol_option = input("Select Network Protocol (TCP/UDP): ")

network_protocol = socket.AF_INET if network_protocol_option == "IPv4" else socket.AF_INET6
transport_protocol = socket.SOCK_STREAM if transport_protocol_option == "TCP" else socket.SOCK_DGRAM
sock = socket.socket(network_protocol, transport_protocol)


server_address = "127.0.0.1" if network_protocol_option == "IPv4" else "::1"
custom_server_address = input("IP address (localhost as default): ")
if custom_server_address != "":
    server_address == custom_server_address

server_port = 7777
# custom_server_port = input("PORT (7777 as default): ")
# if custom_server_port != "":
#     server_port == custom_server_port

if(transport_protocol_option == "TCP"):
    sock.connect((server_address, server_port))

def sendUDP(message):
    encoded_message = bytes(message, encoding='utf8')
    sock.sendto(encoded_message, (server_address, server_port))
    data, addr = sock.recvfrom(4096)
    print("\n[CLIENT]: Response from Server UDP")
    print(data.decode('utf-8'))


def sendTCP(message):
    encoded_message = bytes(message, encoding='utf8')
    sock.send(encoded_message)
    data = sock.recv(4096)
    print("\n[CLIENT]: Response from Server TCP")
    print(data.decode('utf-8'))


def send(message):
    if transport_protocol_option == "UDP":
        sendUDP(message)
    if transport_protocol_option == "TCP":
        sendTCP(message)


message = "Hi Server, I am Client.."
print("\n[CLIENT]: Sending friendly message to the Server")
if transport_protocol_option == "UDP":
    sendUDP(message)
if transport_protocol_option == "TCP":
    sendTCP(message)

command = "null"
while command != "e":
    command = input(
        "\nChoose an option:\n[e] Exit\n[m] Send message\n[d] DNS Translation\n[p] Reveal Server or Proxy IP\n\nYour choice: ")
    if command == "m":
        message = input("Your message: ")
        send(message)
        if(message == "KILL"):
            sock.close()
            sys.exit()
    if command == "d":
        hostnameOrIP = input("\nInsert hostname or IP to translate: ")
        if (hostnameOrIP.replace('.', '').isnumeric()):
            print(f"{hostnameOrIP} translate to {socket.gethostbyaddr(hostnameOrIP)}")
        else:
            print(f"{hostnameOrIP} translate to {socket.gethostbyname(hostnameOrIP)}")
    if command == "p":
        requestServerOrProxyIP = ""
        while requestServerOrProxyIP != "server" and requestServerOrProxyIP != "proxy":
            requestServerOrProxyIP = input("Server[server] IP or the IP of the Proxy[proxy]?\n")

        requestCommandIP = "GETSERVERIP" if requestServerOrProxyIP == "server" else "GETPROXYIP"
        send(requestCommandIP)

sock.close()
