from flask import Flask, request

app = Flask(__name__)

global api_data
api_data = None # Un cassettino per fare la GET e impostarne il valore via POST/PUT

@app.route("/", methods=['GET'])
def GET():
    return f"API Data: {api_data}"

@app.route("/", methods=['POST'])
def POST():
    global api_data
    if(api_data != None):
        return "API Data already defined, use PUT to modify instead"

    api_data = request.data
    return f"New API Data value: {request.data}"

@app.route("/", methods=['PUT'])
def PUT():
    if(api_data == None):
        return "API Data not already defined, use POST to initialize instead"
        
    api_data = request.data
    return f"New API Data value: {request.data}"

if __name__ == '__main__':
    app.run(debug=False)