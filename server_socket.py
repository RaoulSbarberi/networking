import sys
import socket
import requests

# Il Proxy e' semplicemente un 'Man In the Middle' che accetta richieste da un host e le ripropone a quello di destinazione
# ritornando al primo il risultato della richiesta, con la possibilita' di fare quindi da filtro per motivi svariati
# quali: sicurezza, ruoli degli utenti, centralizzazione di un pool di host sotto stesso Proxy ecc..
# Per testare il Proxy utilizzero' questo sito che, tramite una richiesta HTTP, ritorna l'IP del richiedente
# Nel qualcaso venisse fatta la richiesta utilizzando il Proxy questo restituira' l'IP qui sotto anziche' quello del server
# come se la richiesta fosse stata fatta (ed e' stata fatta) dal Proxy anziche' dal nostro client -> nostro server
my_ip_url = "https://httpbin.org/ip"
proxies = {
    "https": "212.191.132.210:8080",
    "http": "212.191.132.210:8080"
}

# START
network_protocol_option = ""  # IPv4 or IPv6
while network_protocol_option != "IPv4" and network_protocol_option != "IPv6":
    network_protocol_option = input("Select Network Protocol (IPv4/IPv6): ")

transport_protocol_option = ""  # TCP or UDP
while transport_protocol_option != "TCP" and transport_protocol_option != "UDP":
    transport_protocol_option = input("Select Network Protocol (TCP/UDP): ")
# IPv4 utilizza 32-bit per descrivere gli indirizzi che viene poi tradotta nel
# "famoso" formato X.X.X.X a 4 byte, mentre IPv6 utilizza ben 128-bit, il formato
# in questo caso e' X:X:X:X:X:X:X:X, 8 word da 16-bit l'una
network_protocol = socket.AF_INET if network_protocol_option == "IPv4" else socket.AF_INET6

# La principale differenza tra UPD/TCP a livello di codice, e' che mentre in TCP viene stabilita
# la connessione dopo un handshaking in UDP non viene stabilita alcuna connessione e vanno pertanto
# specificiati i parametri indirizzo e porta ogni volta che si intende inviare o ricevere dati
transport_protocol = socket.SOCK_STREAM if transport_protocol_option == "TCP" else socket.SOCK_DGRAM

# TODO: La parte decisionale dei protocolli andra' centralizzata in uno script piu' alto
# Il quale agira' da centralizzatore e lancera' sia client che server con i medesimi parametri
# per impostare gli stessi protocolli su ambedue
server_adress = "127.0.0.1" if network_protocol_option == "IPv4" else "::1"
server_port = 7777  # 0 to 1024 prese dal sistema, scelta arbitraria sino 65535
sock = socket.socket(network_protocol, transport_protocol)
sock.bind((server_adress, server_port))

tcp_connection = None
udp_address_and_port = None
if transport_protocol_option == "TCP":
    backlog = 1  # Numero di clients accettabili
    print("Listening for TCP client to connect..")
    sock.listen(backlog)
    tcp_connection, client_address = sock.accept()
    print(f"Established TCP connection to client {client_address}")

print(f"[SERVER]: Connection started on {server_adress}:{server_port}")
print(
    f"[SERVER]: We are using {transport_protocol_option} protocol on {network_protocol_option}")
print("[SERVER]: Send a 'KILL' message from a client to stop it..")


def send(response):
    encodedMsg = bytes(response, encoding='utf8')
    if transport_protocol_option == "UDP":
        sock.sendto(encodedMsg, udp_address_and_port)
    if transport_protocol_option == "TCP":
        tcp_connection.send(encodedMsg)


def processMessage(message):
    if message == "KILL":
        print("Exiting due to client 'KILL' command")
        send("Server is gonna stop")
        sys.exit()
    elif message == "GETSERVERIP":
        print("Server IP requested due to client 'GETSERVERIP' command")
        result = str(requests.get(my_ip_url).json())
        send(result)
    elif message == "GETPROXYIP":
        result = None
        try:
            result = str(requests.get(my_ip_url, proxies=proxies).json())
        except:
            result = "The Proxy is expired or busy.."
        send(result)
        print("Proxy IP requested due to client 'GETPROXYIP' command")
    else:
        echo = f"I've recived your message: {message}"
        send(echo)


def logMessage(message):
    print("[SERVER]: Message from Client")
    print(message)


while True:
    data = None
    if transport_protocol_option == "UDP":
        data, udp_address_and_port = sock.recvfrom(4096)
    if transport_protocol_option == "TCP":
        data = tcp_connection.recv(4096)

    if data == None:
        continue

    message = data.decode('utf-8')
    logMessage(message)
    processMessage(data.decode('utf-8'))
