import requests

# API test
url = "http://127.0.0.1:5000"
custom_url = input("server uri (localhost as default): ")
if custom_url != "":
    url = custom_url

option = ""
while option != "e":
    print("Choose one:\n[e] Exit\n[g] GET\n[p] POST\n[u] PUT")
    option = input("\nYour choice: ")
    if option == "g":
        print(f"\n ==# GET response: {requests.get(url).text} #==\n")
    if option == "p":
        data = input("Insert some value: ")
        print(f"\n ==# POST response: {requests.post(url, data).text} #==\n")
    if option == "u":
        data = input("Insert some value: ")
        print(f"\n ==# PUT response: {requests.put(url, data).text} #==\n")